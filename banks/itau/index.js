var formatters = require('../../lib/formatters'),
  ediHelper = require('../../lib/edi-helper'),
  helper = require('./helper');

exports.options = {
  logoURL: 'data:base/jpg;base64,/9j/4AAQSkZJRgABAgAAZABkAAD/7AARRHVja3kAAQAEAAAARgAA/+4ADkFkb2JlAGTAAAAAAf/bAIQABAMDAwMDBAMDBAYEAwQGBwUEBAUHCAYGBwYGCAoICQkJCQgKCgwMDAwMCgwMDQ0MDBERERERFBQUFBQUFBQUFAEEBQUIBwgPCgoPFA4ODhQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQU/8AAEQgAJgCVAwERAAIRAQMRAf/EAIoAAAEFAQEAAAAAAAAAAAAAAAABBQYHCAQCAQEBAQAAAAAAAAAAAAAAAAAAAQIQAAAFAgQDBAYFBg8AAAAAAAECAwQFEQYAEhMHIRQVMSIWCEFhMkIjF1FxkTQJsTOTJCUY8MFicrLSc4NE1DVWllcZEQEBAQEAAwAAAAAAAAAAAAAAAQIRIUFx/9oADAMBAAIRAxEAPwDUu/2+sTsTa7Sadx55eXlHAtIqLIoCAKHIXOodRUSnykIFK0KIiIgFONQDKRvxCb5EwiSz4kpBHulFdyYQD6BGoV+zAJ/6EX3/ALQiP0rn+tgFD8Qm+gEM1oRIl9IAs5ARD66jgNU+X7fiK33tl7LNo88RNRK5W0tGGUBcpBVLnTUTUApMxDgBqVKAgJRD1iFu4AwBgDAGAMAYAwBgDAGAMAYAwGIPxE/uu3f9pMf0WWAwpgDAGA3T+HZ+Y3G/nw35H2A3AOHELgowBgDAGABwBgDAGAMAYAwBgMj+cq2Wl6XvslaD9ZVuxnZd/HuF2+XWIm4MxIYxM4GLmCvCoDgM8G8p1yS7fcyStFVd41sqaVhoJkqVAVpQGyoFcGMqKqRUzIkEDmqnQ3u4CtmGxm68paAX1HW2q6toWxpAi6S7Y65mZK5lytgV1xTCg94E8B02ZtwcxYOYvOEk3cLeKLhrYbaLWbJGlpgqhUUkjrHUEUEinN3zmJUewPpwGrfIVBy1syO6tvzrYWUzGOIhq/aGMUwpLJg+zFExBMUaeoRxKNCvfMJszG3Evakjd7JpOtnZo1w2caiJU3hDZRSMqoQqYGr9J8audc7IdWWU4G7ONOHDj6K4gQFKj2cPt9HqwC5wp/D+OmATOFaDw7a8fowC5vV9vDAR2Hv20Z5ecbRUs3cK2y5FjPBmFMGjktapqCoBQAQpx9GAkCaxFSFUTEDpnADEMUQMBij2CAh6OOAZ7ruuLs6EWnZYFjtkjJpJN2iRnDpdddQE0kUESAJjqHMIABQ/JUQB3RWFVJNQyZkjHKUxkj0zFEwB3TZREKhWg0HAV+pvttSS7RsYtxJrXKR6nFKt27d04RTfrHBMrdRykidAimYcokMoAlHgNKDgLCOqRMhlFDARMgCY5zDQpQDtERHswEesq/rQ3FhxuCypZCZhgWO1M7bCIlBdMCiYhgMACAgBgHj6BDASTAZV820vH2zuHsTdU2rysDDz7pxIvMpjlSTKLNQREpAMYe6Qw0KAjwwFEM9x9sbl/eCtaVuzw+wv2WSlrZnTtHLhBUjVwZbKKaYAcpj0LwNl4D6qYCZbVbreX6x4q1XUbNRkIA2+qwuRitCOHM8rNKpfEUWkgIfK3zF7pE6gIiHu+yEQsTcHZ6Q272fJd12LW9P7TzC7xeHJHrPDyCS7vXIKShKEIXgTMYREQADd2uWoXn5UZ6Jujc7fi5YJxzcJKzEe7YOwKdMFEVOeEDZVAKYPqMADgKDvl2DNTexafkY91t+13EK6uCyRVKzmpchXJClK0cH1BApRMU6hCJAYSFNRQuJc7vwz7T7zF7sujDetwbeyE9GTlmmhdeRXuM0cwQcPBRUKg3hCmMR3nTE2tqlpXMIVAtMUPu47q7bn3L3xatL8n4a3LStFpPQbGFkDtUhekjTuCGA5MwgmJiiY5UhLqek3dwCyF9XVdjTy42zc12Prdti+YlZ3dFwMHfTnT9+0YpnRbGepiAp6qhu8BTBqCenbSgSjcdd0xvnZzaVpeMw121uBSbNKXESXWGSfuGSZlUGZpUD6wUUNpiAKZjhQnaGAk3lluWemUtxLfkJhzcdu2rdL2HtuffKC6crMU6G0lHI1FwKQmANU1REDcRplAAz9czEH23HmzHOsmZrdRnIaChks2RUvdUyCGYlBETFHgPprgHjcC6o6Ns1laNoTVyOZ+AsQtydXLd60UwaFVA5kjgJTmO+WKqYCEQGpQIBEymDjQJhaW7r1HcbaySvO7RbW9L7VN5mY5t2CDBeXzJHWXOnUEtYABTsLmDiAYCxPKVcM3dmxNuz1wybqXlnLiTBV9IKmXcnKnIuSJgY6giagEAoAHoDgHDhgKtmpKZ8tV5muayJ6Puvay/7rpMWkoqmeVZTEqpprKslUjGFSgp0Eh+IUAohWpwCy99NyWUvtZJwm2j0lwXPdMiNksEYpZFVYr1cg88TMY5SlURagqepzFAo5REQ7cBXexMgbare26dvZK23ljWdeMenclqxUqs0V03UUiVB8Uh2S7hMdRMorGAxgEpU+IdgiGqfEkD4c8XdRb+GOS6r1bVJyvIaWvr6tcunp9/NWlOOAq/zLMtmpHb4jLeqRNEwSzxMsXItyLKPEZDIfKZAqCSxhHJnz1TEuWubAYkNtv5SMw5N6pECV7oGgH5hp6xBsFfswHn5beUr/ALrf/wDH5D/L4BQ228pNQzb1SAl9IBb78Bp9fLDgNneV+P2Ri7LfsdlZY82ySeft2TdprJPVXgplEoqkXRQECgSgJ5UwJ20qObAOD4PK14xcdSGwfH/Om5rmejdY6hn72fP8fWz9ubvVw1L4I7LsDy5eJZXxyNneLuRDrXWem9Q6flLTX1/iZMuWmf3aerAOcSGyGlLdEG19HorXrwNOn08Pcr+q83p/4PlqaWr8LS9nu4DzcgbH+AY8brG2PlfkQ6OEhyHQ8mX4HK6vwKZfzel6OzAcb8PL18t4/qY2j8ptUelcz07oWvqqZtHP8DU1NXNTvZ89eObAS2yQsfwyx+XYxng+g9O6Do8hTMObS5Xue1XNT01rgG1oG03K3cDAbe5PVW8eChyOnrZB1uqZO7myV1OY45fa4YCFNf3TawXKDYVdFz4dy9J+76ivMctX3M+rmy8K5/XgO8geWjp9nZRsvpWqt4D/ANM0OY5n43Tfdz8x7ejx1e3vUwE5ssLG8ONfl2MX4SzLcl4e5fp2bVPraXJ/Drq58+X36144CvIcPK58xVOhDZnzP5pWoNen9V5+o6uTL39eubPl79a141wEmgA2V6i08LmtfrHVJHkemhH8z1nQ/aOlo9/mdH7zl+Jp+33cA6XKG24TMJ4yNCDPgV54c6vynOZNIOd5TX71NKmvpe57XDAddLI8DUrF/LfpPb+rdE6Hy36DldD+70/5OA//2Q==',
  codigo: '341'
};

exports.dvBarra = function (barra) {
  var resto2 = formatters.mod11(barra, 9, 1);
  return (resto2 == 0 || resto2 == 1 || resto2 == 10 || resto2 == 11) ? 1 : 11 - resto2;
};

exports.barcodeData = function (boleto) {
  var codigoBanco = this.options.codigo;
  var numMoeda = "9";

  var fatorVencimento = formatters.fatorVencimento(boleto['data_vencimento']);

  var agencia = formatters.addTrailingZeros(boleto['agencia'], 4);

  var conta = formatters.addTrailingZeros(boleto['codigo_cedente'], 5);

  var valor = formatters.addTrailingZeros(boleto['valor'], 10);
  var carteira = boleto['carteira'];

  var nossoNumero = formatters.addTrailingZeros(boleto['nosso_numero'], 8);

  var barra = codigoBanco + numMoeda + fatorVencimento + valor + carteira + nossoNumero + formatters.mod10(agencia + conta + carteira + nossoNumero) + agencia + conta + formatters.mod10(agencia + conta) + '000';

  var dvBarra = this.dvBarra(barra);
  var lineData = barra.substring(0, 4) + dvBarra + barra.substring(4, barra.length);

  boleto['codigo_cedente'] = [conta, '-', formatters.mod10([agencia, conta].join(''))].join('');
  boleto['nosso_numero'] = carteira + '/' + nossoNumero;
  boleto['nosso_numero_dv'] = formatters.mod10([agencia, conta, carteira, nossoNumero].join(''));

  return lineData;
};

exports.linhaDigitavel = function (barcodeData) {
  // 01-03    -> Código do banco sem o digito
  // 04-04    -> Código da Moeda (9-Real)
  // 05-05    -> Dígito verificador do código de barras
  // 06-09    -> Fator de vencimento
  // 10-19    -> Valor Nominal do Título
  // 20-44    -> Campo Livre (Abaixo)
  // 20-24    -> Código da Agencia (sem dígito)
  // 25-27    -> Número da Carteira
  // 28-36    -> Nosso Número (sem dígito)
  // 37-43    -> Conta do Cedente (sem dígito)
  // 44-44    -> Zero (Fixo)

  var campos = [];

  // 1. Campo - composto pelo código do banco, código da moéda, as cinco primeiras posições
  // do campo livre e DV (modulo10) deste campo
  var campo = barcodeData.substr(0, 3) + barcodeData.substr(3, 1) + barcodeData.substr(19, 3) + barcodeData.substr(22, 2);
  campo = campo + formatters.mod10(campo);
  campo = campo.substr(0, 5) + '.' + campo.substr(5);
  campos.push(campo);

  // 2. Campo - composto pelas posiçoes 6 a 15 do campo livre
  // e livre e DV (modulo10) deste campo
  campo = barcodeData.substr(24, 6) + barcodeData.substr(30, 1) + barcodeData.substr(31, 3);
  campo = campo + formatters.mod10(campo);
  campo = campo.substr(0, 5) + '.' + campo.substr(5);
  campos.push(campo);

  // 3. Campo composto pelas posicoes 16 a 25 do campo livre
  // e livre e DV (modulo10) deste campo
  campo = barcodeData.substr(34, 1) + barcodeData.substr(35, 6) + barcodeData.substr(41, 3);
  campo = campo + formatters.mod10(campo);
  campo = campo.substr(0, 5) + '.' + campo.substr(5);
  campos.push(campo);

  // 4. Campo - digito verificador do codigo de barras
  campo = barcodeData.substr(4, 1);
  campos.push(campo);

  // 5. Campo composto pelo fator vencimento e valor nominal do documento, sem
  // indicacao de zeros a esquerda e sem edicao (sem ponto e virgula). Quando se
  // tratar de valor zerado, a representacao deve ser 000 (tres zeros).
  campo = barcodeData.substr(5, 4) + barcodeData.substr(9, 10);
  campos.push(campo);

  return campos.join(" ");
};

exports.parseEDIFile = function (fileContent) {
  console.log('Not implemented');
};